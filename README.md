Put files that are needed for your config in files

Run src/install.sh to install whatever you need to setup.

Run src/link.sh to link your config to your home dir.

Vim Manual  

Windows:
<C-H> and <C-L> are used to move between buffers in a window  
<C-c> is to close a buffer
gt and gT are used to move between workspaces  
<space>h, k, j, l is used to move between windows  

Completion:
<C-N>, <C-P> to scroll through autocomplete
<space>d jumps to def, <C-O> jumps back  
<Space>g typical goto function
<Space>r Renaming 
<Space>n Usages (shows all the usages of a name)

<S-~> Toggle terminal
<C-N>, <C-P>, <C-X> Multiple cursors
  A, I, c, s, v within multiple cursors
<S>ll to start compiling latex
