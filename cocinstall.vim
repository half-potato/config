" Source this file within vim
" https://github.com/neoclide/coc.nvim/wiki/Using-coc-extensions
CocInstall coc-vimtex
CocInstall coc-jedi
CocInstall coc-ultisnips
CocInstall coc-css
CocInstall coc-cmake
CocInstall coc-clangd
CocInstall coc-cmake
CocInstall coc-cmake
CocInstall coc-cmake
