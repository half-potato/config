set termguicolors

" Install plug if its not already installed
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source ~/.config/nvim/init.vim
endif

call plug#begin('~/.local/share/nvim/plugged')
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' } " <C-n> to toggle the file browser
Plug 'tpope/vim-surround'
Plug 'scrooloose/nerdcommenter'
Plug 'kien/rainbow_parentheses.vim'
Plug 'tweekmonster/gofmt.vim'

" Excellent Latex support
Plug 'sirver/ultisnips'
    let g:UltiSnipsExpandTrigger = '<tab>'
    let g:UltiSnipsJumpForwardTrigger = '<tab>'
    let g:UltiSnipsJumpBackwardTrigger = '<s-tab>'

Plug 'lervag/vimtex'
    let g:tex_flavor='latex'
    let g:vimtex_view_method='zathura'
    let g:vimtex_quickfix_mode=0

Plug 'KeitaNakamura/tex-conceal.vim'
    set conceallevel=1
    let g:tex_conceal='abdmg'
    hi Conceal ctermbg=none

" Adds auto complete
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}
Plug 'cespare/vim-toml'
" Plug 'psliwka/vim-smoothie'
Plug 'ashisha/image.vim'

" Fuzzy find
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'mhinz/vim-grepper'

" Plug 'honza/vim-snippets'

Plug 'tpope/vim-fugitive' " git support
Plug 'zah/nim.vim' " Nim support. Don't use nim the errors are garbage
Plug 'morhetz/gruvbox'

Plug 'rust-lang/rust.vim'
Plug 'vhdirk/vim-cmake' " Should use CMake to figure out how to autocomplete

Plug 'racer-rust/vim-racer' " rust support
Plug 'dgraham/vim-eslint' " I forget
Plug 'tikhomirov/vim-glsl'

" Plug 'w0rp/ale' " adds our linter

Plug 'zefei/vim-wintabs' " adds more tabs to each window
Plug 'zefei/vim-wintabs-powerline' " makes those tabs visible in powerline
Plug 'mhinz/vim-startify' " adds our startup window

"Plug 'flazz/vim-colorschemes' " more colorschemes
Plug 'majutsushi/tagbar' " adds fancy outline. press <F8>
Plug 'vim-airline/vim-airline' " adds status bar
Plug 'vim-airline/vim-airline-themes' " adds status bar
Plug 'kshenoy/vim-signature' " shows marks
Plug 'ryanoasis/vim-devicons' " fancy icons
call plug#end()
filetype plugin on
filetype indent on
syntax sync fromstart

" Snippets
fun! SetupVAM()
  let c = get(g:, 'vim_addon_manager', {})
  let g:vim_addon_manager = c
  let c.plugin_root_dir = expand('$HOME', 1) . '/.vim/vim-addons'

  " Force your ~/.vim/after directory to be last in &rtp always:
  " let g:vim_addon_manager.rtp_list_hook = 'vam#ForceUsersAfterDirectoriesToBeLast'

  " most used options you may want to use:
  " let c.log_to_buf = 1
  " let c.auto_install = 0
  let &rtp.=(empty(&rtp)?'':',').c.plugin_root_dir.'/vim-addon-manager'
  if !isdirectory(c.plugin_root_dir.'/vim-addon-manager/autoload')
    execute '!git clone --depth=1 git://github.com/MarcWeber/vim-addon-manager '
        \       shellescape(c.plugin_root_dir.'/vim-addon-manager', 1)
  endif

  " This provides the VAMActivate command, you could be passing plugin names, too
  call vam#ActivateAddons([], {})
endfun
call SetupVAM()

ActivateAddons vim-snippets snipmate
let g:snipMate = { 'snippet_version' : 1 }

nnoremap <buffer> <F9> :exec '!python3' shellescape(@%, 1)<cr>
" Use tab to go through Deoplete menu
inoremap <expr><TAB>  pumvisible() ? "\<C-n><Enter>" : "\<TAB>"

" Run spell checking on text files (i.e md, txt, etc)
au BufReadPost,BufNewFile *.md,*.txt setlocal spell spelllang=en_US

set completeopt-=preview

" C autocomplete can be great, but C++ is hopeless
let g:clang_compilation_database = "./build"
let g:clang_c_completeopt = 'menuone,preview,noselect,noinsert'
let g:inccomplete_autoselect = 0

" Rainbow parens
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces

" Nerd Tree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
map <C-n> :NERDTreeToggle<CR>

" Vanilla vim conf
"colorscheme abbott
let g:gruvbox_contrast_dark='hard'
colorscheme gruvbox
set mouse=a
set hidden
set visualbell
set autoindent                                                                   
set expandtab                                                                    
set tabstop=2                                                                    
set shiftwidth=2
set nolinebreak
set number
let mapleader="\<Space>"
let maplocalleader="\<Space>"

let g:lightline = {
      \ 'colorscheme': 'gruvbox',
      \ }

" Move between split windows using space+hkjl
nmap <silent> <leader>h :wincmd h<CR>
nmap <silent> <leader>k :wincmd k<CR>
nmap <silent> <leader>j :wincmd j<CR>
nmap <silent> <leader>l :wincmd l<CR>
nmap <silent> <C-_> <Plug>NERDCommenterToggle
vmap <silent> <C-_> <Plug>NERDCommenterToggle

" FZF Fuzzy File search bindings
nnoremap ,g :FZF<CR>
nnoremap ,f :Rg<CR>
let g:grepper={}
let g:grepper.tools=["rg"]

xmap gr <plug>(GrepperOperator)
nnoremap <leader>R
      \ :let @s='\<'.expand('<cword>').'\>'<CR>
      \ :Grepper -cword -noprompt<CR>
      \ :cfdo %s/<C-r>s// \| update
      \<Left><Left><Left><Left><Left><Left><Left><Left><Left><Left>

" ==============================================================================
" BEGIN Terminal Config
" ==============================================================================
tnoremap <Esc> <C-\><C-n>
set splitbelow

" Function for ability to toggle a popup terminal using ~
function! ChooseTerm(termname, slider)
	let pane = bufwinnr(a:termname)
	let buf = bufexists(a:termname)
	if pane > 0
		" pane is visible
		if a:slider > 0
			:exe pane . "wincmd c"
		else
			:exe "e #" 
		endif
	elseif buf > 0
		" buffer is not in pane
		if a:slider
			:exe "topleft split"
		endif
		:exe "buffer " . a:termname
	else
		" buffer is not loaded, create
		if a:slider
			:exe "topleft split"
		endif
		:terminal
		:exe "f " a:termname
	endif
endfunction

" Toggle 'default' terminal
nnoremap ~ :call ChooseTerm("term-slider", 1)<CR>

" ==============================================================================
" END Terminal Config
" ==============================================================================

" Bindings for WinTabs: Allows for multiple buffers in a window
map <C-H> <Plug>(wintabs_previous)
map <C-L> <Plug>(wintabs_next)
map <C-C> <Plug>(wintabs_close)
command! Tabc WintabsCloseVimtab
command! Tabo WintabsOnlyVimtab

" Bindings for ALE: Allows for cycling through errors
nnoremap <leader>n :lnext<CR>
nnoremap <leader>p :lprevious<CR>
nnoremap <leader>r :lrewind<CR>

let g:jedi#completions_enabled = 0
let g:python_highlight_all = 1

" Save undos between sessions
silent !mkdir ~/.vim/backups > /dev/null 2>&1
set undodir=~/.vim/backups
set undofile

" Ultisnips: Allows for autocompletion of multiline constructs
let g:UltiSnipsJumpForwardTrigger="<tab>"

" ROS Launch files are xml
au BufRead,BufNewFile *.launch set filetype=xml
au BufRead,BufNewFile *.cl set filetype=c

" Reopen vim at same location
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
    \| exe "normal! g'\"" | endif
endif

" Nerd commenter config
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDToggleCheckAllLines = 1
let g:NERDSpaceDelims = 1
let g:NERDCustomDelimiters={ 'ino' : { 'leftAlt': '/**','rightAlt': '*/', 'left' : '//'} }

" Change how comments work
au FileType c,cpp setlocal comments-=:// comments+=f://
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Tagbar toggle to give attributes for sections of code
nmap <F8> :TagbarToggle<CR>
let g:vimtex_compiler_progname = 'nvr'

" Vimtex
if empty(v:servername) && exists('*remote_startserver')
  call remote_startserver('VIM')
endif
let g:matchup_override_vimtex = 1
let g:vimtex_compiler_latexmk = {
        \ 'backend' : 'nvim',
        \ 'background' : 1,
        \ 'build_dir' : '',
        \ 'callback' : 1,
        \ 'continuous' : 1,
        \ 'executable' : 'latexmk',
        \ 'options' : [
        \   '-pdf',
        \   '-file-line-error',
        \   '-shell-escape',
        \   '-synctex=1',
        \   '-interaction=nonstopmode',
        \ ],
        \}

" ==============================================================================
" BEGIN COC Config
" ==============================================================================

" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
if has('patch8.1.1068')
  " Use `complete_info` if your (Neo)Vim version supports it.
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  imap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current line.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Introduce function text object
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use <TAB> for selections ranges.
" NOTE: Requires 'textDocument/selectionRange' support from the language server.
" coc-tsserver, coc-python are the examples of servers that support it.
nmap <silent> <TAB> <Plug>(coc-range-select)
xmap <silent> <TAB> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings using CoCList:
" Show all diagnostics.
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
" nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
" nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

" ==============================================================================
" END COC Config
" ==============================================================================

" ----------------------------------------------------------------------------
" vim-fugitive {{
" ----------------------------------------------------------------------------
nmap     <Leader>gs :Gstatus<CR>gg<c-n>
nnoremap <Leader>d :Gdiff<CR>
" }}
let g:tex_flavor = 'latex'
let g:tex_noindent_env = 'bmatrix\|align*\|align'
